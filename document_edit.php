<?php
$active = "subjects";
include "header.php";

if (!isset($_SESSION["tentk"]) || $_SESSION["vaitro"] != 0 || !isset($_GET["id"])) {
    include "error.php";
    return;
}

$id = $_GET["id"];

if (isset($_POST["submit"])) {
    $mamon = $_POST["mamon"];
    $tieude = $_POST["tieude"];
    $url = $_POST["link"];
    $sql = "update tailieu set tieude='$tieude', link='$url' where matailieu='$id'";
    mysqli_query($link, $sql);
    echo mysqli_error($link);
    header("Location: documents.php?id=$mamon");
} else {
    $sql = "select * from tailieu where matailieu=$id";
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_assoc($result);
    if ($row == null) {
        include "error.php";
        return;
    }
}
?>

    <div class="panel panel-default">
        <div class="panel-heading">
            Chỉnh sửa tài liệu
        </div>
        <div class="panel-body">
            <form method="post">
                <input type="hidden" name="mamon" value="<?= $row["mamon"] ?>">

                <div class="form-group">
                    <label class="required">Tên tài liệu</label>
                    <input class="form-control" name="tieude" value="<?= $row["tieude"] ?>" maxlength="200"
                           required></div>

                <div class="form-group">
                    <label class="required">Đường dẫn</label>
                    <input class="form-control" name="link" value="<?= $row["link"] ?>" required></div>

                <input type="submit" name="submit" value="Cập nhật" class="btn btn-success">
                <a href="documents.php?id=<?= $row["mamon"] ?>" class="btn btn-default">Trở về</a>
            </form>
        </div>
    </div>

<?php
include "footer.php";