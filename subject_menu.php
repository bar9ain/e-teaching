<?php
$active = "subjects";
include "header.php";

if (!isset($_SESSION["tentk"]) && !isset($_GET["id"])) {
    include "error.php";
    return;
}
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <?php
        $id = $_GET["id"];
        $sql = "select tenmon from monhoc where mamon=$id";
        $query = mysqli_query($link, $sql);
        $row = mysqli_fetch_assoc($query);
        echo $row["tenmon"];
        ?>
    </div>
    <div class="panel-body">
        <div class="row items">
            <div class="col-md-3">
                <a href="subject_description.php?id=<?= $id ?>">
                    <img src="img/info.png">
                    <h5>Giới thiệu môn học</h5>
                </a>
            </div>
            <div class="col-md-3">
                <a href="documents.php?id=<?= $id ?>">
                    <img src="img/document.png">
                    <h5>Tài liệu tham khảo</h5>
                </a>
            </div>
            <div class="col-md-3">
                <a href="courses.php?id=<?= $id ?>">
                    <img src="img/courses.png">
                    <h5>Danh sách bài học</h5>
                </a>
            </div>
            <div class="col-md-3">
                <a href="exercises.php?id=<?= $id ?>">
                    <img src="img/excercise.png">
                    <h5>Thi trắc nghiệm</h5>
                </a>
            </div>
            <div class="col-md-3">
                <a href="announcements.php?id=<?= $id ?>">
                    <img src="img/bell.png">
                    <h5>Thông báo</h5>
                </a>
            </div>
            <div class="col-md-3">
                <a href="subject_students">
                    <img src="img/members.png">
                    <h5>Danh sách học viên</h5>
                </a>
            </div>
            <div class="col-md-3">
                <a href="homework.php?id=<?= $id ?>">
                    <img src="img/homework.png">
                    <h5>Bài tập về nhà</h5>
                </a>
            </div>
            <div class="col-md-3">
                <a href="gradebook.php?id=<?= $id ?>">
                    <img src="img/gradebook.png">
                    <h5>Sổ điểm</h5>
                </a>
            </div>
        </div>
    </div>
</div>