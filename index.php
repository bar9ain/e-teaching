<?php
$active = "home";
include "header.php"; ?>

    <div class="row">

        <?php
        if (isset($_SESSION["tentk"])) {
            $tentk = $_SESSION["tentk"];
            ?>

            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center"><?= $_SESSION["hoten"] ?></div>
                            <div class="panel-body">
                                <?php $image = file_exists("img/avatar/$tentk.jpg") ? "img/avatar/$tentk.jpg" : "img/unknown.jpg"; ?>
                                <img src="<?= $image ?>" style="display: block; margin: auto; max-width: 160px;"></div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">Thông tin cá nhân</div>
                            <div class="panel-body">
                                <div class="list-group">
                                    <a href="subjects.php" class="list-group-item">
                                        <img src="img/book.png"> Các môn học của tôi</a>
                                    <a href="profile.php" class="list-group-item">
                                        <img src="img/user.png"> Sửa thông tin cá nhân</a>
                                    <a href="profile_password.php" class="list-group-item">
                                        <img src="img/password.png"> Đổi mật khẩu</a>
                                    <a href="logout.php" class="list-group-item">
                                        <img src="img/logout.png"> Đăng xuất</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
        } else {
            ?>

            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Đăng nhập</div>
                    <div class="panel-body">
                        <form action="login.php" method="post">
                            <div class="form-group">
                                <label class="required">Tên đăng nhập</label>
                                <input class="form-control" type="text" name="tentk" required></div>

                            <div class="form-group">
                                <label class="required">Mật khẩu</label>
                                <input class="form-control" type="password" name="matkhau" required></div>

                            <div class="submit-container">
                                <input class="btn btn-success btn-block" type="submit" name="login" value="Đăng nhập">
                            </div>
                        </form>
                    </div>

                </div>
            </div>

            <?php
        }
        ?>

        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <img class="img" src="img/BG.jpg">
                </div>
            </div>
        </div>

    </div>
<?php

include "footer.php";
?>