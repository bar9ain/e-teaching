<?php
$active = "subjects";
include "header.php";

if (!isset($_SESSION["tentk"]) || $_SESSION["vaitro"] != 0 || !isset($_GET["id"])) {
    include "error.php";
    return;
}

if(isset($_GET["id"])) {
    $id = $_GET["id"];
    $sql = "delete from baihoc where mabaihoc=$id";
    mysqli_query($link, $sql);
    echo "<div class='alert alert-success'>Bài học đã xóa!</div>";
}

include "footer.php";