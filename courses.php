<?php
$active = "subjects";
include "header.php";

if (!isset($_SESSION["tentk"]) && !isset($_GET["id"])) {
    include "error.php";
    return;
}
$id = $_GET["id"];
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <?php
        $sql = "select tenmon from monhoc where mamon=$id";
        $query = mysqli_query($link, $sql);
        $row = mysqli_fetch_assoc($query);
        echo "<a href='subject_menu.php?id=$id'>" . $row["tenmon"] . "</a>";
        ?>
        > Danh sách bài học
    </div>

    <div class="panel-body">
        <?php
        $tentk = $_SESSION["tentk"];

        // Trang giáo viên
        if ($_SESSION["vaitro"] == 0) {
            $mamon = $_GET["id"];
            $sql = "SELECT * FROM baihoc WHERE mamon='$mamon'";
            $query = mysqli_query($link, $sql);
            $list = array();
            while ($row = mysqli_fetch_array($query)) {
                $list[] = $row;
            }
            ?>

            <div class="row">
                <div class="col-md-12">
                    <a href="course_new.php?mamon=<?= $mamon ?>" class="btn btn-success">Tạo bài học mới</a>
                    <hr>
                </div>
            </div>
            <div class="subjects-list">

                <?php
                foreach ($list as $item) { ?>

                    <div class="subject-row">
                        <div class="avatar-container">
                            <img src="img/courses.png">
                        </div>
                        <div class="subject-details">
                            <h3 class="subject-name">
                                <a href="course_view.php?id=<?= $item["mabaihoc"] ?>">
                                    <span class="project-full-name"><?= $item["tenbaihoc"] ?></span></a>
                            </h3>
                            <div class="subject-description">
                                <p>Lượt xem: <?= $item["luottai"] ?></p>
                            </div>
                        </div>
                        <div class="controls">
                            <a href="course_edit.php?id=<?= $item["mabaihoc"] ?>" class="btn btn-default">Sửa</a>
                            <a href="course_delete.php?id=<?= $item["mabaihoc"] ?>" class="btn btn-danger"
                               onclick='return confirm("Xóa bài này?")'>Xóa</a>
                        </div>
                    </div>

                    <?php
                }
                ?>

            </div>

            <?php
        } // Trang sinh viên
        else {
        ?>
        <div class="subjects-list">

            <?php
            $mamon = $_GET["id"];
            $sql = "SELECT * FROM baihoc 
            WHERE mamon='$mamon'";
            $query = mysqli_query($link, $sql);
            $list = array();
            while ($row = mysqli_fetch_array($query)) {
                $list[] = $row;
            }
            ?>
            <div class="subjects-list">

                <?php
                foreach ($list as $item) { ?>

                    <div class="subject-row">
                        <div class="avatar-container">
                            <img src="img/courses.png">
                        </div>
                        <div class="subject-details">
                            <h3 class="subject-name">
                                <a href="course_view.php?id=<?= $item["mabaihoc"] ?>">
                                    <span class="project-full-name"><?= $item["tenbaihoc"] ?></span></a>
                            </h3>
                            <div class="subject-description">
                                <p>Lượt xem: <?= $item["luottai"] ?></p>
                            </div>
                        </div>
                    </div>

                    <?php
                }
                ?>

            </div>

            <?php
            } ?>

        </div>

        <?php

        ?>
    </div>
</div>