-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 27, 2018 at 11:14 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elearning`
--

-- --------------------------------------------------------

--
-- Table structure for table `baihoc`
--

DROP TABLE IF EXISTS `baihoc`;
CREATE TABLE IF NOT EXISTS `baihoc` (
  `mabaihoc` int(11) NOT NULL AUTO_INCREMENT,
  `mamon` int(11) NOT NULL,
  `tenbaihoc` varchar(200) COLLATE utf8_vietnamese_ci NOT NULL,
  `link` text COLLATE utf8_vietnamese_ci NOT NULL,
  `luottai` int(11) NOT NULL,
  PRIMARY KEY (`mabaihoc`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `baihoc`
--

INSERT INTO `baihoc` (`mabaihoc`, `mamon`, `tenbaihoc`, `link`, `luottai`) VALUES
(1, 1, 'Bài 1: Giới thiệu SQL và SQL Server', 'https://www.youtube.com/watch?v=2fanjSYVElY&list=PL33lvabfss1xnFpWQF6YH11kMTS1HmLsw', 4),
(3, 1, 'Bài 2: Tạo database', 'https://www.youtube.com/watch?v=XUIm5VQlpJM&list=PL33lvabfss1xnFpWQF6YH11kMTS1HmLsw&index=1', 1),
(4, 1, 'Bài 3: Tạo bảng', 'https://www.youtube.com/watch?v=TrLKdQH_Qng&list=PL33lvabfss1xnFpWQF6YH11kMTS1HmLsw&index=2', 1),
(5, 1, 'Bài 4: Kiểu dữ liệu trong SQL', 'https://www.youtube.com/watch?v=_40bzGOHloo&list=PL33lvabfss1xnFpWQF6YH11kMTS1HmLsw&index=3', 1),
(8, 2, 'Bài 1: Giới thiệu', 'asdasd', 2);

-- --------------------------------------------------------

--
-- Table structure for table `baitap`
--

DROP TABLE IF EXISTS `baitap`;
CREATE TABLE IF NOT EXISTS `baitap` (
  `mabaitap` int(11) NOT NULL AUTO_INCREMENT,
  `mamon` int(11) NOT NULL,
  `tieude` varchar(300) COLLATE utf8_vietnamese_ci NOT NULL,
  `link` text COLLATE utf8_vietnamese_ci NOT NULL,
  `luottai` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mabaitap`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `baitap`
--

INSERT INTO `baitap` (`mabaitap`, `mamon`, `tieude`, `link`, `luottai`) VALUES
(1, 1, 'Bài tập chương 1', 'http://google.com/', 3),
(2, 1, 'Bài tập chương 2', 'https://www.youtube.com/watch?v=2fanjSYVElY&list=PL33lvabfss1xnFpWQF6YH11kMTS1HmLsw', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cauhoitracnghiem`
--

DROP TABLE IF EXISTS `cauhoitracnghiem`;
CREATE TABLE IF NOT EXISTS `cauhoitracnghiem` (
  `macauhoi` int(11) NOT NULL AUTO_INCREMENT,
  `mabaithi` int(11) NOT NULL,
  `noidungcauhoi` text COLLATE utf8_vietnamese_ci NOT NULL,
  PRIMARY KEY (`macauhoi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

-- --------------------------------------------------------

--
-- Table structure for table `danhsachhocvien`
--

DROP TABLE IF EXISTS `danhsachhocvien`;
CREATE TABLE IF NOT EXISTS `danhsachhocvien` (
  `mamon` int(11) NOT NULL,
  `mahocvien` varchar(30) COLLATE utf8_vietnamese_ci NOT NULL,
  `trangthai` int(11) NOT NULL,
  PRIMARY KEY (`mamon`,`mahocvien`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dapantracnghiem`
--

DROP TABLE IF EXISTS `dapantracnghiem`;
CREATE TABLE IF NOT EXISTS `dapantracnghiem` (
  `madapan` int(11) NOT NULL AUTO_INCREMENT,
  `macauhoi` int(11) NOT NULL,
  `noidungdapan` text COLLATE utf8_vietnamese_ci NOT NULL,
  `dapandung` int(11) NOT NULL,
  PRIMARY KEY (`madapan`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

-- --------------------------------------------------------

--
-- Table structure for table `monhoc`
--

DROP TABLE IF EXISTS `monhoc`;
CREATE TABLE IF NOT EXISTS `monhoc` (
  `mamon` int(11) NOT NULL AUTO_INCREMENT,
  `tenmon` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL,
  `giangvien` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `thongtin` text COLLATE utf8_vietnamese_ci NOT NULL,
  PRIMARY KEY (`mamon`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `monhoc`
--

INSERT INTO `monhoc` (`mamon`, `tenmon`, `giangvien`, `thongtin`) VALUES
(1, 'Cơ sở dữ liệu', 'admin', 'Cơ sở dữ liệu là một tập hợp thông tin có cấu trúc. Tuy nhiên, thuật ngữ này thường dùng trong công nghệ thông tin và nó thường được hiểu rõ hơn dưới dạng một tập hợp liên kết các dữ liệu, thường đủ lớn để lưu trên một thiết bị lưu trữ như đĩa hay băng. Dữ liệu này được duy trì dưới dạng một tập hợp các tập tin trong hệ điều hành hay được lưu trữ trong các hệ quản trị cơ sở dữ liệu.'),
(2, 'Cơ sở dữ liệu nâng cao', 'admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `sodiem`
--

DROP TABLE IF EXISTS `sodiem`;
CREATE TABLE IF NOT EXISTS `sodiem` (
  `mahocvien` varchar(30) COLLATE utf8_vietnamese_ci NOT NULL,
  `mabaithi` int(11) NOT NULL,
  `diem` float NOT NULL,
  `magiangvien` varchar(30) COLLATE utf8_vietnamese_ci NOT NULL,
  `thoigian` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mahocvien`,`mabaithi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tailieu`
--

DROP TABLE IF EXISTS `tailieu`;
CREATE TABLE IF NOT EXISTS `tailieu` (
  `matailieu` int(11) NOT NULL AUTO_INCREMENT,
  `mamon` int(11) NOT NULL,
  `tieude` varchar(300) COLLATE utf8_vietnamese_ci NOT NULL,
  `link` text COLLATE utf8_vietnamese_ci NOT NULL,
  `luottai` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`matailieu`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `tailieu`
--

INSERT INTO `tailieu` (`matailieu`, `mamon`, `tieude`, `link`, `luottai`) VALUES
(1, 2, '', 'http://google.com', 0),
(2, 2, 'Chương 1', 'http://google.com', 0),
(3, 1, 'Chương 1: Giới thiệu', 'https://www.youtube.com/watch?v=2fanjSYVElY&list=PL33lvabfss1xnFpWQF6YH11kMTS1HmLsw', 7);

-- --------------------------------------------------------

--
-- Table structure for table `thitracnghiem`
--

DROP TABLE IF EXISTS `thitracnghiem`;
CREATE TABLE IF NOT EXISTS `thitracnghiem` (
  `maibaithi` int(11) NOT NULL AUTO_INCREMENT,
  `mamon` int(11) NOT NULL,
  `tenbaithi` varchar(300) COLLATE utf8_vietnamese_ci NOT NULL,
  `ngaytao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `thoihan` datetime NOT NULL,
  PRIMARY KEY (`maibaithi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

-- --------------------------------------------------------

--
-- Table structure for table `thongbao`
--

DROP TABLE IF EXISTS `thongbao`;
CREATE TABLE IF NOT EXISTS `thongbao` (
  `mathongbao` int(11) NOT NULL AUTO_INCREMENT,
  `magiangvien` varchar(30) COLLATE utf8_vietnamese_ci NOT NULL,
  `mamon` int(11) NOT NULL,
  `noidung` text COLLATE utf8_vietnamese_ci NOT NULL,
  `thoigian` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mathongbao`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `tentk` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `matkhau` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_vietnamese_ci NOT NULL,
  `hodem` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `ten` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `sdt` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `ngaysinh` date DEFAULT NULL,
  `vaitro` int(11) NOT NULL,
  PRIMARY KEY (`tentk`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`tentk`, `matkhau`, `email`, `hodem`, `ten`, `sdt`, `ngaysinh`, `vaitro`) VALUES
('145D4802010012', '145D4802010012', 'abc@gmail.com', 'Nguyễn Văn', 'An', '0978756466', '1995-08-08', 1),
('admin', '123', 'admin@gmail.com', 'Phan Anh', 'Phong', '0168797998', '1968-08-01', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
