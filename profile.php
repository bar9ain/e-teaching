<?php
$active = "profile";
include "header.php";

if (!isset($_SESSION["tentk"])) {
    include "error.php";
    return;
}

$tentk = $_SESSION["tentk"];
$sql = "SELECT * FROM user WHERE tentk='$tentk'";
$check = mysqli_query($link, $sql);
if (mysqli_num_rows($check) > 0) {
    $row = mysqli_fetch_assoc($check);
    $hodem = $row["hodem"];
    $ten = $row["ten"];
    $email = $row["email"];
    $sdt = $row["sdt"];
    $ngaysinh = $row["ngaysinh"];
    $vaitro = $row["vaitro"];
} else
    header("Location: index.php");

?>

    <div class="row">
        <div class="col-lg-4">
            <h4 class="prepend-top-0">
                Hình đại diện
            </h4>
            <p>
                Hãy chọn cho mình bức hình đẹp nhất.
            </p>
        </div>
        <div class="col-lg-8">
            <form action="update.php" method="post" enctype="multipart/form-data">
                <div class="img-avatar">
                    <?php $image = file_exists("img/avatar/$tentk.jpg") ? "img/avatar/$tentk.jpg" : "img/unknown.jpg"; ?>
                    <img src="<?= $image ?>"></div>
                <div class="form-group">
                    <label>Chọn ảnh</label>
                    <input accept="image/*" type="file" name="avatar">
                    <div class="help-block">Kích thước tối đa cho phép là 200KB.</div>
                </div>
                <hr>
                <input type="submit" name="submit_avatar" class="btn btn-success" value="Cập nhật">
            </form>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-lg-4">
            <h4>
                Thông tin cá nhân</h4>
            <p>
                Thông tin cơ bản của bạn.</p>
        </div>
        <div class="col-lg-8">
            <form action="update.php" method="post">
                <div class="form-group">
                    <label class="required">Họ đệm</label>
                    <input class="form-control" name="hodem" value="<?= $hodem ?>" required>
                </div>
                <div class="form-group">
                    <label class="required">Tên</label>
                    <input class="form-control" name="ten" value="<?= $ten ?>" required>
                </div>
                <div class="form-group">
                    <label>Tên tài khoản</label>
                    <input class="form-control" value="<?= $tentk ?>" readonly disabled>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" name="email" value="<?= $email ?>">
                </div>
                <div class="form-group">
                    <label>Số điện thoại</label>
                    <input class="form-control" name="sdt" value="<?= $sdt ?>">
                </div>
                <div class="form-group">
                    <label>Ngày sinh</label>
                    <input class="form-control" name="ngaysinh" type="date" value="<?= $ngaysinh ?>">
                </div>
                <div class="form-group">
                    <label>Vai trò: </label>
                    <input class="form-control" value="<?= $vaitro == 0 ? "Giáo viên" : "Sinh viên" ?>" readonly
                           disabled>
                </div>
                <div>
                    <input type="submit" name="update" value="Cập nhật" class="btn btn-success">
                    <a class="btn btn-default" href="index.php">Hủy</a>
                </div>
            </form>
        </div>
    </div>

    <hr>

<?php include "footer.php"; ?>