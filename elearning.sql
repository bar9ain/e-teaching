-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 22, 2018 at 03:53 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elearning`
--

-- --------------------------------------------------------

--
-- Table structure for table `baihoc`
--

DROP TABLE IF EXISTS `baihoc`;
CREATE TABLE IF NOT EXISTS `baihoc` (
  `mabaihoc` int(11) NOT NULL AUTO_INCREMENT,
  `mamon` int(11) NOT NULL,
  `tenbaihoc` varchar(200) COLLATE utf8_vietnamese_ci NOT NULL,
  `link` text COLLATE utf8_vietnamese_ci NOT NULL,
  PRIMARY KEY (`mabaihoc`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `baihoc`
--

INSERT INTO `baihoc` (`mabaihoc`, `mamon`, `tenbaihoc`, `link`) VALUES
(1, 1, 'Bài 1: Giới thiệu SQL và SQL Server', 'https://www.youtube.com/watch?v=2fanjSYVElY&list=PL33lvabfss1xnFpWQF6YH11kMTS1HmLsw'),
(3, 1, 'Bài 2: Tạo database', 'https://www.youtube.com/watch?v=XUIm5VQlpJM&list=PL33lvabfss1xnFpWQF6YH11kMTS1HmLsw&index=1'),
(4, 1, 'Bài 3: Tạo bảng', 'https://www.youtube.com/watch?v=TrLKdQH_Qng&list=PL33lvabfss1xnFpWQF6YH11kMTS1HmLsw&index=2'),
(5, 1, 'Bài 4: Kiểu dữ liệu trong SQL', 'https://www.youtube.com/watch?v=_40bzGOHloo&list=PL33lvabfss1xnFpWQF6YH11kMTS1HmLsw&index=3'),
(8, 2, 'B1', 'asdasd');

-- --------------------------------------------------------

--
-- Table structure for table `monhoc`
--

DROP TABLE IF EXISTS `monhoc`;
CREATE TABLE IF NOT EXISTS `monhoc` (
  `mamon` int(11) NOT NULL AUTO_INCREMENT,
  `tenmon` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL,
  `giangvien` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  PRIMARY KEY (`mamon`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `monhoc`
--

INSERT INTO `monhoc` (`mamon`, `tenmon`, `giangvien`) VALUES
(1, 'Cơ sở dữ liệu', 'admin'),
(2, 'Cơ sở dữ liệu nâng cao', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `tentk` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `matkhau` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_vietnamese_ci NOT NULL,
  `hodem` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `ten` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `sdt` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `vaitro` int(11) NOT NULL,
  PRIMARY KEY (`tentk`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`tentk`, `matkhau`, `email`, `hodem`, `ten`, `sdt`, `vaitro`) VALUES
('145D4802010012', '145D4802010012', 'abc@gmail.com', 'Nguyễn Văn', 'An', '0978756466', 1),
('admin', '123', 'admin@gmail.com', 'Phan Anh', 'Phong', '0168797998', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
