<?php
$active = "subjects";
include "header.php";

if (!isset($_SESSION["tentk"]) && !isset($_GET["id"])) {
    include "error.php";
    return;
}
$id = $_GET["id"];
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <?php
        $sql = "select tenmon from monhoc where mamon=$id";
        $query = mysqli_query($link, $sql);
        $row = mysqli_fetch_assoc($query);
        echo "<a href='subject_menu.php?id=$id'>" . $row["tenmon"] . "</a>";
        ?>
        > Bài tập về nhà
    </div>

    <div class="panel-body">
        <?php
        $tentk = $_SESSION["tentk"];

        // Trang giáo viên
        if ($_SESSION["vaitro"] == 0) {
            $sql = "SELECT * FROM baitap WHERE mamon='$id'";
            $query = mysqli_query($link, $sql);
            $list = array();
            while ($row = mysqli_fetch_array($query)) {
                $list[] = $row;
            }
            ?>

            <div class="row">
                <div class="col-md-12">
                    <a href="homework_new.php?mamon=<?= $id ?>" class="btn btn-success">Tạo bài tập mới</a>
                    <hr>
                </div>
            </div>
            <div class="subjects-list">

                <?php
                foreach ($list as $item) { ?>

                    <div class="subject-row">
                        <div class="avatar-container">
                            <img src="img/homework.png">
                        </div>
                        <div class="subject-details">
                            <h3 class="subject-name">
                                <a href="homework_view.php?id=<?= $item["mabaitap"] ?>">
                                    <span class="project-full-name"><?= $item["tieude"] ?></span></a>
                            </h3>
                            <div class="subject-description">
                                <p>Lượt xem: <?= $item["luottai"] ?></p>
                            </div>
                        </div>
                        <div class="controls">
                            <a href="homework_edit.php?id=<?= $item["mabaitap"] ?>" class="btn btn-default">Sửa</a>
                            <a href="homework_delete.php?id=<?= $item["mabaitap"] ?>" class="btn btn-danger"
                               onclick='return confirm("Xóa bài tập này?")'>Xóa</a>
                        </div>
                    </div>

                    <?php
                }
                ?>

            </div>

            <?php
        } // Trang sinh viên
        else {
        ?>
        <div class="subjects-list">

            <?php
            $id = $_GET["id"];
            $sql = "SELECT * FROM baitap 
            WHERE mamon='$id'";
            $query = mysqli_query($link, $sql);
            $list = array();
            while ($row = mysqli_fetch_array($query)) {
                $list[] = $row;
            }
            ?>
            <div class="subjects-list">

                <?php
                foreach ($list as $item) { ?>

                    <div class="subject-row">
                        <div class="avatar-container">
                            <img src="img/homework.png">
                        </div>
                        <div class="subject-details">
                            <h3 class="subject-name">
                                <a href="homework_view.php?id=<?= $item["mabaitap"] ?>">
                                    <span class="project-full-name"><?= $item["tieude"] ?></span></a>
                            </h3>
                            <div class="subject-description">
                                <p>Lượt xem: <?= $item["luottai"] ?></p>
                            </div>
                        </div>
                    </div>

                    <?php
                }
                ?>

            </div>

            <?php
            } ?>

        </div>

        <?php

        ?>
    </div>
</div>