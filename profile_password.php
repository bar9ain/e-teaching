<?php
$active = "profile";
include "header.php";

if (!isset($_SESSION["tentk"])) {
    include "error.php";
    return;
}
?>

    <div class="row">
        <div class="col-lg-4">
            <h4>
                Thông tin cá nhân</h4>
            <p>
                Thay đổi mật khẩu.</p>
        </div>
        <div class="col-lg-8">

            <?php

            if (isset($_POST["doimatkhau"])) {
                // Kiểm tra mật khẩu xác nhận có trùng khớp mật khẩu mới
                if ($_POST["matkhaumoi"] != $_POST["matkhauxacnhan"]) {
                    echo "<div class='alert alert-danger'>Mật khẩu không trùng khớp!</div>";
                } // Kiểm tra mật khẩu cũ có đúng
                else {
                    $tentk = $_SESSION["tentk"];
                    $sql = "select matkhau from user where tentk='$tentk'";
                    $result = mysqli_query($link, $sql);
                    $row = mysqli_fetch_assoc($result);
                    $matkhaucu = $row["matkhau"];
                    if ($_POST["matkhaucu"] != $matkhaucu) {
                        echo "<div class='alert alert-danger'>Mật khẩu cũ không đúng!</div>";
                    } else {
                        $matkhaumoi = $_POST["matkhaumoi"];
                        $sql = "update user set matkhau='$matkhaumoi' where tentk='$tentk'";
                        mysqli_query($link, $sql);
                        echo "<div class='alert alert-success'>Mật khẩu được đã thay đổi!</div>";
                    }
                }
            }

            ?>

            <form method="post">
                <div class="form-group">
                    <label class="required">Mật khẩu cũ</label>
                    <input class="form-control" type="password" name="matkhaucu" required>
                </div>

                <div class="form-group">
                    <label class="required">Mật khẩu mới</label>
                    <input class="form-control" type="password" name="matkhaumoi" required>
                </div>

                <div class="form-group">
                    <label class="required">Xác nhận mật khẩu</label>
                    <input class="form-control" type="password" name="matkhauxacnhan" required>
                </div>

                <div>
                    <input type="submit" name="doimatkhau" value="Cập nhật" class="btn btn-success">
                    <a class="btn btn-default" href="index.php">Hủy</a>
                </div>
            </form>
        </div>
    </div>

<?php
include "footer.php";