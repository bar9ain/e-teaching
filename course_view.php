<?php
$active = "subjects";
include "header.php";

if (!isset($_SESSION["tentk"]) || !isset($_GET["id"])) {
    include "error.php";
    return;
}

$id = $_GET["id"];
$sql = "select * from baihoc where mabaihoc=$id";
$result = mysqli_query($link, $sql);
$row = mysqli_fetch_assoc($result);
$title = $row["tenbaihoc"];
$url = $row["link"];

// Cập nhật số lượt xem, lượt tải
$sql = "update baihoc set luottai=luottai + 1 where mabaihoc=$id";
mysqli_query($link, $sql);

if (strpos($url, "youtube") !== false) {
    ?>
    <div class="panel panel-danger">
        <div class="panel-heading"><?= $title ?></div>
        <div class="panel-body">
            <iframe width="100%" height="600"
                    src="https://www.youtube.com/embed/'<?= substr($url, strrpos($url, "/") + 1) ?>" frameborder="0"
                    allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
    </div>
    <?php
} else header("Location:$url");

include "footer.php";