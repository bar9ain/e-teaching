<?php
$active = "subjects";
include "header.php";

if (!isset($_SESSION["tentk"]) || $_SESSION["vaitro"] != 0 || !isset($_GET["id"])) {
    include "error.php";
    return;
}

$id = $_GET["id"];

if (isset($_POST["submit"])) {
    $tenmon = $_POST["tenmon"];
    $sql = "update monhoc set tenmon='$tenmon' where mamon='$id'";
    mysqli_query($link, $sql);
    header("Location: subjects.php");
}
else {
    $sql = "select * from monhoc where mamon=$id";
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_assoc($result);
    if ($row == null) {
        include "error.php";
        return;
    }
}
?>

    <div class="panel panel-default">
        <div class="panel-heading">
            Cập nhật môn học
        </div>
        <div class="panel-body">
            <form method="post">
                <div class="form-group">
                    <label>Tên môn</label>
                    <input class="form-control" name="tenmon" value="<?= $row["tenmon"] ?>" maxlength="200" autofocus required>
                </div>
                <input type="submit" name="submit" value="Cập nhật" class="btn btn-success">
                <a href="subjects.php" class="btn btn-default">Trở về</a>
            </form>
        </div>
    </div>

<?php
include "footer.php";