<?php
$active = "subjects";
include "header.php";

if (!isset($_SESSION["tentk"]) || $_SESSION["vaitro"] != 0 || !isset($_GET["id"])) {
    include "error.php";
    return;
}

$mabaihoc = $_GET["id"];

if (isset($_POST["submit"])) {
    $mamon = $_POST["mamon"];
    $tenbaihoc = $_POST["tenbaihoc"];
    $url = $_POST["link"];
    $sql = "update baihoc set tenbaihoc='$tenbaihoc', link='$url' where mabaihoc='$mabaihoc'";
    mysqli_query($link, $sql);
    echo mysqli_error($link);
    header("Location: courses.php?id=$mamon");
} else {
    $sql = "select * from baihoc where mabaihoc=$mabaihoc";
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_assoc($result);
    if ($row == null) {
        include "error.php";
        return;
    }
}
?>

    <div class="panel panel-default">
        <div class="panel-heading">
            Chỉnh sửa bài học
        </div>
        <div class="panel-body">
            <form method="post">
                <input type="hidden" name="mamon" value="<?= $row["mamon"] ?>">

                <div class="form-group">
                    <label class="required">Tên bài học</label>
                    <input class="form-control" name="tenbaihoc" value="<?= $row["tenbaihoc"] ?>" maxlength="200"
                           required></div>

                <div class="form-group">
                    <label class="required">Đường dẫn</label>
                    <input class="form-control" name="link" value="<?= $row["link"] ?>" required></div>

                <input type="submit" name="submit" value="Cập nhật" class="btn btn-success">
                <a href="courses.php?id=<?= $row["mamon"] ?>" class="btn btn-default">Trở về</a>
            </form>
        </div>
    </div>

<?php
include "footer.php";