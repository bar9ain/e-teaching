<?php
$active = "subjects";
include "header.php";

if (!isset($_SESSION["tentk"])) {
    include "error.php";
    return;
}

$id = $_GET["id"];

if (isset($_POST["thongtin"])) {
    $thongtin = $_POST["thongtin"];
    $sql = "update monhoc set thongtin='$thongtin' where mamon=$id";
    mysqli_query($link, $sql);
    header("location:subject_menu.php?id=$id");
}
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <?php
        $sql = "select tenmon, thongtin from monhoc where mamon=$id";
        $query = mysqli_query($link, $sql);
        $row = mysqli_fetch_assoc($query);
        echo $row["tenmon"];
        ?>
    </div>
    <div class="panel-body">
        <?php if ($_SESSION["vaitro"] == 1): ?>
            <h5>Giới thiệu môn học</h5>
            <div class="well">
                <?= $row["thongtin"] ?>
            </div>
        <?php else: ?>
            <form method="post">
                <div class="form-group">
                    <label>Giới thiệu môn học</label>
                    <textarea class="form-control" name="thongtin" rows="10"><?= $row["thongtin"] ?></textarea>
                    <hr>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">Cập nhật</button>
                        <a href="subject_menu.php?id=<?= $id ?>" class="btn btn-default">Hủy bỏ</a>
                    </div>
                </div>
            </form>
        <?php endif; ?>
    </div>
</div>