<?php
$active = "subjects";
include "header.php";

if (!isset($_SESSION["tentk"]) || $_SESSION["vaitro"] != 0 || !isset($_GET["mamon"])) {
    include "error.php";
    return;
}

$id = $_GET["mamon"];

if (isset($_POST["submit"])) {
    $tenbaihoc = $_POST["tenbaihoc"];
    $url = $_POST["link"];
    $sql = "insert into baihoc(mamon, tenbaihoc, link) values ($id, '$tenbaihoc', '$url')";
    mysqli_query($link, $sql);
    header("Location: courses.php?id=$id");
}
?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <?php
            $sql = "select tenmon from monhoc where mamon=$id";
            $query = mysqli_query($link, $sql);
            $row = mysqli_fetch_assoc($query);
            echo "<a href='subject_menu.php?id=$id'>" . $row["tenmon"] . "</a>";
            ?>
            > <a href="courses.php?id=<?= $id ?>">Danh sách bài học</a>
            > Tạo bài học mới
        </div>
        <div class="panel-body">
            <form method="post">
                <div class="form-group">
                    <label class="required">Tên bài học</label>
                    <input class="form-control" name="tenbaihoc" maxlength="200" required></div>

                <div class="form-group">
                    <label class="required">Đường dẫn</label>
                    <input class="form-control" name="link" required></div>

                <input type="submit" name="submit" value="Tạo" class="btn btn-success">
            </form>
        </div>
    </div>

<?php
include "footer.php";