<?php
$active = "subjects";
include "header.php";

if (!isset($_SESSION["tentk"]) || $_SESSION["vaitro"] != 0 || !isset($_GET["id"])) {
    include "error.php";
    return;
}

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    $sql = "delete from monhoc where mamon=$id";
    mysqli_query($link, $sql);
    header("Location: subjects.php");
}

include "footer.php";
