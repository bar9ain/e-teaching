<?php
$active = "subjects";
include "header.php";

if (!isset($_SESSION["tentk"])) {
    include "error.php";
    return;
}
?>

    <div class="panel panel-default">
        <div class="panel-heading">
            Các môn học của tôi
        </div>

        <div class="panel-body">

            <?php

            $tentk = $_SESSION["tentk"];

            // Nếu vai trò là giảng viên
            if ($_SESSION["vaitro"] == 0) {
                $sql = "SELECT * FROM monhoc
                        inner join `user` on monhoc.giangvien=`user`.tentk
                        WHERE monhoc.giangvien='$tentk'";
                $query = mysqli_query($link, $sql);
                $list = array();
                while ($row = mysqli_fetch_array($query)) {
                    $list[] = $row;
                }
                ?>

                <div class="row">
                    <div class="col-md-12">
                        <a href="subject_new.php" class="btn btn-success">Tạo môn học mới</a>
                        <hr>
                    </div>
                </div>
                <div class="subjects-list">

                    <?php
                    foreach ($list as $item) {
                        $href = "subject_menu.php?id=" . $item["mamon"];
                        ?>

                        <div class="subject-row">
                            <div class="avatar-container">
                                <a href="<?= $href ?>">
                                    <img src="img/book-open-icon.png">
                                </a>
                            </div>
                            <div class="subject-details">
                                <h3 class="subject-name">
                                    <a href="<?= $href ?>">
                                        <span class="project-full-name"><?= $item["tenmon"] ?></span></a>
                                </h3>
                            </div>
                            <div class="controls">
                                <a href="subject_edit.php?id=<?= $item["mamon"] ?>" class="btn btn-default">Sửa</a>
                                <a href="subject_delete.php?id=<?= $item["mamon"] ?>" class="btn btn-danger"
                                   onclick='return confirm("Xóa môn học này?")'>Xóa</a>
                            </div>
                        </div>

                        <?php
                    }
                    ?>
                </div>
                <?php

            } // Nếu là sinh viên
            else {
                $sql = "SELECT * FROM monhoc inner join `user` on monhoc.giangvien=`user`.tentk";
                $query = mysqli_query($link, $sql);
                $list = array();
                while ($row = mysqli_fetch_array($query)) {
                    $list[] = $row;
                }

                echo "<div class='subjects-list'>";
                foreach ($list as $item) {
                    $href = "subject_menu.php?id=" . $item["mamon"];
                    ?>

                    <div class="subject-row">
                        <div class="avatar-container">
                            <a href="<?= $href ?>">
                                <img src="img/book-open-icon.png">
                            </a>
                        </div>
                        <div class="subject-details">
                            <h3 class="subject-name">
                                <a href="<?= $href ?>">
                                    <span class="project-full-name"><?= $item["tenmon"] ?></span></a>
                            </h3>
                            <div class="subject-description">
                                <p>Giảng viên: <?= $item["hodem"] . " " . $item["ten"] ?></p>
                            </div>
                        </div>
                    </div>

                    <?php
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>

<?php
include "footer.php";
