<?php
session_start();
include "csdl.php";
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/sb-admin-2.css">
    <link rel="stylesheet" href="css/style.css">
    <title>E-Learning</title>
</head>
<body>
<div class="container">

    <div class="header">
        <img src="img/header-logo.png">
    </div>

    <div class="top-area">
        <ul class="nav nav-links">
            <li class="<?= $active == "home" ? "active" : null ?>">
                <a href="index.php">
                    Trang chủ
                </a></li>
            <?php if (isset($_SESSION["tentk"])): ?>
                <li class="<?= $active == "subjects" ? "active" : null ?>">
                    <a href="subjects.php">
                        Các môn học của tôi</a></li>
                <li class="<?= $active == "profile" ? "active" : null ?>">
                    <a href="profile.php">
                        Sửa thông tin cá nhân</a></li>
                <li>
                    <a href="logout.php"
                       target="_top">Đăng xuất</a></li>
            <?php endif; ?>
        </ul>
    </div>