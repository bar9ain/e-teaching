<?php
$active = "subjects";
include "header.php";

if (!isset($_SESSION["tentk"]) || $_SESSION["vaitro"] != 0) {
    include "error.php";
    return;
}

if (isset($_POST["submit"])) {
    $tenmon = $_POST["tenmon"];
    $giangvien = $_SESSION["tentk"];
    $sql = "insert into monhoc(tenmon, giangvien) values ('$tenmon', '$giangvien')";
    mysqli_query($link, $sql);
    header("Location: subjects.php");
}
?>

    <div class="panel panel-default">
        <div class="panel-heading">
            Tạo môn học mới
        </div>
        <div class="panel-body">
            <form method="post">
                <div class="form-group">
                    <label>Tên môn</label>
                    <input class="form-control" name="tenmon" maxlength="200" autofocus required>
                </div>
                <input type="submit" name="submit" value="Tạo môn học" class="btn btn-success">
                <a href="subjects.php" class="btn btn-default">Trở về</a>
            </form>
        </div>
    </div>

<?php
include "footer.php";